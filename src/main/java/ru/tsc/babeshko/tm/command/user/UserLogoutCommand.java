package ru.tsc.babeshko.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.tsc.babeshko.tm.enumerated.Role;

public final class UserLogoutCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "logout";

    @NotNull
    public static final String DESCRIPTION = "Log out";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        serviceLocator.getAuthService().logout();
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}