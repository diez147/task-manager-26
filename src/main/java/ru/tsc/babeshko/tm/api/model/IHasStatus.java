package ru.tsc.babeshko.tm.api.model;

import org.jetbrains.annotations.NotNull;
import ru.tsc.babeshko.tm.enumerated.Status;

public interface IHasStatus {

    @NotNull
    Status getStatus();

    void setStatus(@NotNull Status status);

}